import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('order-form', 'Integration | Component | order form', {
  integration: true
});

test('renders the title', function(assert) {

  const title = 'Some Random TITLE';

  this.set('title', title);

  this.render(hbs`{{order-form title=title}}`);

  assert.equal(this.$('h2').text().trim(), title);
});

test('renders the description', function(assert) {

  const chocolatePerBar = 10;

  this.set('chocolatePerBar', chocolatePerBar);

  this.render(hbs`{{order-form chocolatePerBar=chocolatePerBar}}`);

  assert.equal(this.$('p').text().trim(), `We only accept payment in chocolate. We can produce 1 bar per ${chocolatePerBar} grams of chocolate.`);
});

test('renders a number input with the value of chocolate', function(assert) {

  const chocolate = 70;

  this.set('chocolate', chocolate);

  this.render(hbs`{{order-form chocolate=chocolate}}`);

  assert.equal(this.$('input[type="number"]').val(), chocolate);
});

test('renders an update button', function(assert) {

  this.render(hbs`{{order-form}}`);

  assert.equal(this.$('button').text().trim(), 'UPDATE');
});

test('renders an auto-update checkbox that is unchecked by default', function(assert) {

  this.render(hbs`{{order-form}}`);

  assert.equal(this.$('input[type="checkbox"]').length, 1);
  assert.equal(this.$('input[type="checkbox"]').is(':checked'), false);
});

test('renders the number of bars', function(assert) {

  const chocolatePerBar = 10;
  const chocolate = 71;
  const bars = 7;

  this.set('chocolatePerBar', chocolatePerBar);
  this.set('chocolate', chocolate);

  this.render(hbs`{{order-form chocolatePerBar=chocolatePerBar chocolate=chocolate}}`);

  assert.equal(this.$('h3').text().trim(), `Bars you will get: ${bars}`);
});

test('renders -1 if bars is smaller than 1', function(assert) {

  const chocolatePerBar = 10;
  const chocolate = 9;
  const bars = -1;

  this.set('chocolatePerBar', chocolatePerBar);
  this.set('chocolate', chocolate);

  this.render(hbs`{{order-form chocolatePerBar=chocolatePerBar chocolate=chocolate}}`);

  assert.equal(this.$('h3').text().trim(), `Bars you will get: ${bars}`);
});

test('changing the input value and clicking the button changes chocolate', function(assert) {

  const chocolatePerBar = 10;
  const chocolate = 55;
  const newInputValue = 75;
  const mockUpdateChocolateAmount = newValue => assert.equal(newValue, newInputValue);

  this.set('chocolatePerBar', chocolatePerBar);
  this.set('chocolate', chocolate);
  this.set('updateChocolateAmount', mockUpdateChocolateAmount);

  this.render(hbs`{{order-form 
    chocolatePerBar=chocolatePerBar 
    chocolate=chocolate
    updateChocolateAmount=updateChocolateAmount
    }}`);

  this.$('input[type="number"]').val(newInputValue).change();
  this.$('button').click();
});

test('changing chocolate updates the number of bars (1+)', function(assert) {

  const chocolatePerBar = 10;
  const chocolate = 55;
  const newChocolate = 73;
  const newBars = 7;

  this.set('chocolatePerBar', chocolatePerBar);
  this.set('chocolate', chocolate);

  this.render(hbs`{{order-form 
    chocolatePerBar=chocolatePerBar 
    chocolate=chocolate
    }}`);

  this.set('chocolate', newChocolate);

  assert.equal(this.$('h3').text().trim(), `Bars you will get: ${newBars}`);
});

test('changing chocolate updates the number of bars (-1)', function(assert) {

  const chocolatePerBar = 10;
  const chocolate = 55;
  const newChocolate = 3;
  const newBars = -1;

  this.set('chocolatePerBar', chocolatePerBar);
  this.set('chocolate', chocolate);

  this.render(hbs`{{order-form 
    chocolatePerBar=chocolatePerBar 
    chocolate=chocolate
    }}`);

  this.set('chocolate', newChocolate);

  assert.equal(this.$('h3').text().trim(), `Bars you will get: ${newBars}`);
});

test('changing the input value to a number lower than chocolatePerBar triggers window.alert', function(assert) {

  const chocolatePerBar = 10;
  const chocolate = 55;
  const newInputValue = 9;
  const mockUpdateChocolateAmount = () => {};
  const errorMsg = `Invalid input. 
Please enter a chocolate amount larger than or 
equal to ${chocolatePerBar} grams.`;

  this.set('chocolatePerBar', chocolatePerBar);
  this.set('chocolate', chocolate);
  this.set('updateChocolateAmount', mockUpdateChocolateAmount);

  this.render(hbs`{{order-form 
    chocolatePerBar=chocolatePerBar 
    chocolate=chocolate
    updateChocolateAmount=updateChocolateAmount
    }}`);

  window.alert = msg => assert.equal(msg, errorMsg);

  this.$('input[type="number"]').val(newInputValue).change();
  this.$('button').click();
});

// TODO: The tests above only cover what was asked of me. They do not cover the auto-update feature.
