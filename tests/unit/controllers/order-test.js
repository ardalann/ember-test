import { moduleFor, test } from 'ember-qunit';

moduleFor('controller:order', 'Unit | Controller | order', {
  needs: ['storage:order']
});

test('has a chocolatePerBar property that equals 5', function(assert) {
  let controller = this.subject();
  assert.equal(controller.chocolatePerBar, 5);
});

test('has an order property of type object', function(assert) {
  let controller = this.subject();
  assert.equal(typeof controller.order, 'object');
});

test('has a updateChocolateAmount action', function(assert) {
  let controller = this.subject();
  assert.equal(typeof controller.actions.updateChocolateAmount, 'function');
});

test('calling updateChocolateAmount action changes order.chocolate', function(assert) {
  let controller = this.subject();
  const value = 123;
  controller.send('updateChocolateAmount', value);
  assert.equal(controller.get('order.chocolate'), value);
});
