# ember-test

My first hands-on experience with Ember.

## Overview

Given that a chocolate factory produces one chocolate bar with exactly 5g of chocolate, create an ember application that takes in chocolate in grams and displays the total number of chocolate bars produced. If there's not enough chocolate provided to produce a bar,  its display -1 and notifies the user through an alert. The application's state should be preserved upon page refresh.

## Example output 

- input value of 8 grams => 1 bar
- input value of 20 grams => 4 bars
- input value of 3 grams => -1

## Testing

Provide unit and integration tests

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with NPM)
* [Bower](https://bower.io/)
* [Ember CLI](https://ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* `cd ember-test`
* `npm install`
* `bower install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](https://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
