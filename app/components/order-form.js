import Ember from 'ember';

const numeric = value => (!value || isNaN(value)) ? 0 : parseInt(value);

export default Ember.Component.extend({
  init() {
    this._super();

    this.set('inputValue', this.chocolate);
  },

  inputValue: 0,
  autoUpdate: false,
  bars: Ember.computed('chocolate', 'chocolatePerBar', function() {
    return Math.floor(this.chocolate / this.chocolatePerBar) || -1;
  }),
  actions: {
    calculateBars() {
      const numericValue = numeric(this.inputValue);
      this.updateChocolateAmount(numericValue);
      if (!this.autoUpdate && numericValue < this.chocolatePerBar) {
        window.alert(`Invalid input. 
Please enter a chocolate amount larger than or 
equal to ${this.chocolatePerBar} grams.`);
      }
    },
    inputValueChanged(newValue) {
      if (this.autoUpdate) {
        this.updateChocolateAmount(numeric(newValue));
      }
    },
  },
});
