import Ember from 'ember';
import { storageFor } from 'ember-local-storage';

export default Ember.Controller.extend({
  order: storageFor('order'),

  chocolatePerBar: 5, // grams

  actions: {
    updateChocolateAmount(newValue) {
      this.set('order.chocolate', newValue);
    }
  },
});
